﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public interface IAppliedPromocodesService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}