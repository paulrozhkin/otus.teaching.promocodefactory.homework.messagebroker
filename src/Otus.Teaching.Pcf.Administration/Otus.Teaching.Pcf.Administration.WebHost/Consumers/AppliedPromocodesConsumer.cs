﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using Otus.Teaching.Pcf.Integration.Core.Messages;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class AppliedPromocodesConsumer: IConsumer<GivePromoCodeToCustomerMessage>
    {
        private readonly IAppliedPromocodesService _appliedPromocodesService;

        public AppliedPromocodesConsumer(IAppliedPromocodesService appliedPromocodesService)
        {
            _appliedPromocodesService = appliedPromocodesService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerMessage> context)
        {
            var message = context.Message;
            if (message.PartnerManagerId == null)
            {
                return;
            }

            await _appliedPromocodesService.UpdateAppliedPromocodesAsync(message.PartnerManagerId.Value);
        }
    }
}
