﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public interface IGivePromoCodesService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeInfo givePromoCode);
    }
}