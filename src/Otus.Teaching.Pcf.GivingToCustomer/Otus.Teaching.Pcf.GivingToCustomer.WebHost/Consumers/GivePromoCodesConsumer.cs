﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using Otus.Teaching.Pcf.Integration.Core.Messages;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivePromoCodesConsumer : IConsumer<GivePromoCodeToCustomerMessage>
    {
        private readonly IGivePromoCodesService _givePromoCodesService;

        public GivePromoCodesConsumer(IGivePromoCodesService givePromoCodesService)
        {
            _givePromoCodesService = givePromoCodesService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerMessage> context)
        {
            var givePromoCode = new GivePromoCodeInfo(context.Message);
            await _givePromoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(givePromoCode);
        }
    }
}
