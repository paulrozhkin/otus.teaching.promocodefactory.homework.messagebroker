﻿using System;
using Otus.Teaching.Pcf.Integration.Core.Messages;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class GivePromoCodeInfo
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public GivePromoCodeInfo()
        {
        }

        public GivePromoCodeInfo(GivePromoCodeRequest request)
        {
            ServiceInfo = request.ServiceInfo;
            PartnerId = request.PartnerId;
            PromoCodeId = request.PromoCodeId;
            PromoCode = request.PromoCode;
            PreferenceId = request.PreferenceId;
            BeginDate = request.BeginDate;
            EndDate = request.EndDate;
        }

        public GivePromoCodeInfo(GivePromoCodeToCustomerMessage toCustomerMessage)
        {
            ServiceInfo = toCustomerMessage.ServiceInfo;
            PartnerId = toCustomerMessage.PartnerId;
            PromoCodeId = toCustomerMessage.PromoCodeId;
            PromoCode = toCustomerMessage.PromoCode;
            PreferenceId = toCustomerMessage.PreferenceId;
            BeginDate = toCustomerMessage.BeginDate;
            EndDate = toCustomerMessage.EndDate;
        }
    }
}